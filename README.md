[![pipeline status](https://gitlab.com/ShawnStephens517/stig_rhel_mattermost_container/badges/master/pipeline.svg)](https://gitlab.com/ShawnStephens517/stig_rhel_mattermost_container/-/commits/master) 
# Mattermost Team Edition
Version 5.25.1

This project is for those who are looking to use Mattermost Enterprise Edition without enabling the paid features. 
If you are looking for the DCARS Mattermost Enterprise Edition, it is located here: 

https://repo1.dsop.io/dsop/opensource/mattermost/mattermost.


## Notes
Adapted from https://github.com/mattermost/mattermost-docker. This repository 
contains files which you can use if you wish to follow a Docker-based 
implementation of Mattermost Team Edition. The Iron Bank provides hardened images 
for Postgres and NGINX server which should be utilized. It is necessary to pass 
in configuration files for both the database and the proxy server. 

It is imperative if implementing Mattermost with the docker-compose.yml file from the 
mattermost-docker repository to change the `image:` reference for both the 
proxy server and database to use the Iron Bank hardened images. 

It is also possible to utilize this image in a Kubernetes-based deployment
of Mattermost. You must simply substitute the Iron Bank version of the Mattermost
Team Edition image into the Operator deployment files instead of the public
Mattermost Team Edition container.

### Configuration
Mattermost provides a checklist for those looking to utilize the Mattermost 
application. Many of the steps for the Enterprise Edition apply to the Team
Edition rollout as well, with the exception of needing a license. 

The checklist, along with resources, can be found here: 

https://docs.mattermost.com/getting-started/enterprise-roll-out-checklist.html.

You should configure the Mattermost Enterprise Edition container with the 
following environment variables:

Application container runs the Mattermost application. You should configure it with following environment variables :

`MM_USERNAME`: database username
`MM_PASSWORD`: database password
`MM_DBNAME`: database name
If your database use some custom host and port, it is also possible to configure them :

`DB_HOST`: database host address
`DB_PORT_NUMBER`: database port
If you use a Mattermost configuration file on a different location than the default one (/mattermost/config/config.json) :

`MM_CONFIG`: configuration file location inside the container.

Configuration file information is located here and details how you must pass in 
the configuration file from your system: 

https://docs.mattermost.com/administration/config-settings.html.

### Running the Container
In order to run the Mattermost Team Edition container, you must download the 
.tar.gz file from the Iron Bank which contains the hardened container.

- Navigate to the directory where the tarball was downloaded.
- Run `podman load -i` on the tarball in order to build.
- Run `podman run` on the image that was built and pass in the aforementioned 
environment variables. 

The Mattermost container utilizes the following ports: 8065 8067 8074 8075