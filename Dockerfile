###############################################################
FROM registry.gitlab.com/shawnstephens517/stig_rhel8_container:development

RUN curl -O https://releases.mattermost.com/5.25.1/mattermost-5.25.1-linux-amd64.tar.gz
RUN curl -O https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
COPY scripts/entrypoint.sh /

ENV PATH="/mattermost/bin:${PATH}"

# Create the Mattermost storage directory.
# The storage directory will contain all the files and images that 
# your users post to Mattermost, so you need to make sure that the 
# drive is large enough to hold the anticipated number of uploaded files and images.
RUN tar xvzf mattermost-5.25.1-linux-amd64.tar.gz && \
    rm -f mattermost-5.25.1-linux-amd64.tar.gz && \
    chmod +x /entrypoint.sh && \
    mkdir -p /mattermost/data /mattermost/plugins /mattermost/client/plugins && \
    cp /mattermost/config/config.json /config.json.save && \
    rm -rf /mattermost/config/config.json && \
    groupadd --system --gid=2000 mattermost && \
    useradd --system --uid=2000 --gid=2000 mattermost && \
    chown -R mattermost:mattermost /mattermost /config.json.save /mattermost/plugins /mattermost/client/plugins && \
    mv jq-linux64 /tmp/jq-linux64 && \
    cp /tmp/jq-linux64 /usr/bin/jq && \
    chmod +x /usr/bin/jq && \
    rm -f /tmp/jq-release.key && \
    rm -f /tmp/jq-linux64.asc && \
    rm -f /tmp/jq-linux64 && \
    dnf upgrade -y && \
    dnf clean all && \
    rm -rf /var/cache/dnf

USER mattermost

# Healthcheck
HEALTHCHECK --interval=5m --timeout=3s \
        CMD curl -f http://localhost:8065/api/v4/system/ping || exit 1

ENTRYPOINT ["/entrypoint.sh"]
WORKDIR /mattermost
CMD ["mattermost"]

EXPOSE 8065 8067 8074 8075

VOLUME ["/mattermost/data", "/mattermost/logs", "/mattermost/config", "/mattermost/plugins", "/mattermost/client/plugins"]
